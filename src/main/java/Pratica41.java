
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;


public class Pratica41 {
    public static void main(String[] args) {
        Elipse elipse = new Elipse(10,5);
        Circulo circulo = new Circulo(3,10,5);
        System.out.println(elipse.getArea());
        System.out.println(elipse.getPerimetro());
        System.out.println(circulo.getArea());
        System.out.println(circulo.getPerimetro());
    }
}
